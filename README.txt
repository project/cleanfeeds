
Copyright 2006 http://2bits.com

This module cleans all the HTML tags from RSS Feeds that you 
provide on your site.

The type of nodes to clean HTML from is selectable.

The idea here is to make sure that interested feed subscribers 
would click to your site to visit links.

Installation
------------
To install this module, upload or copy the directory called "cleanfeeds"
and all the files in it to your modules directory.

If you are using Drupal 4.7.x, then you need to apply a patch to the
node.module. A prepatched version of that module for 4.7.3 is provided
for convenience.

Configuration
-------------
To enable this module do the following:

1. Go to Administer -> Site Building -> Modules, and click enable.

2. Go to Administer -> Site Configuration -> Cleanfeeds and select the types of nodes
   you want to be cleaned in feeds.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
<a href="http://baheyeldin.com">Khalid Baheyeldin</a> of <a href="http://2bits.com">2bits.com</a>.

The author can also be contacted for paid customizations of this module as well as Drupal consulting,
installation, development, and customizations.
